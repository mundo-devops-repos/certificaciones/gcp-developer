# Examen de Certificación Professional Cloud Developer
Readme donde dejo mis notas del examen de certificación de Professional Cloud Developer 
Donde dejo como es el examen, tips, examen de práctica y como aprobar la certificación

Ref: https://cloud.google.com/learn/certification/guides/cloud-developer

Los videos que hice están en mis redes en formato vertical, solo dejo los links de Tiktok porque no estan separados en partes

Redes:
- Youtube: https://www.youtube.com/c/MundoDevOps
- Tiktok: https://www.tiktok.com/@mundo.devops
- Instagram: https://www.instagram.com/mundo.devops/​
- Kick: https://kick.com/mundodevops

[video instructivo hecho por mí](https://www.tiktok.com/@mundo.devops/video/7283897726243310853?lang=es)

### Inscribirse al examen
1. https://webassessor.com/googlecloud
2. [video instructivo hecho por mí](https://www.tiktok.com/@mundo.devops/video/7283994515252317445?lang=es)

### Precio
1. 200 $
2. [video instructivo hecho por mí](https://www.tiktok.com/@mundo.devops/video/7285382149606296838?lang=es)

### Idioma
1. Ingles y Japones
2. [video instructivo hecho por mí](https://www.tiktok.com/@mundo.devops/video/7285762189984632069?lang=es)

### Materiales de estudio que yo use, tú puedes usar cuál te funcione mejor y tu metodo
1. Documentación oficial 
    - Guide https://cloud.google.com/learn/certification/guides/cloud-developer
3. Libro físico y digital 
    - https://github.com/priyankavergadia/GCPSketchnote
3. Exámenes de práctica
    - https://www.examtopics.com/exams/google/professional-cloud-engineer/
    - https://www.whizlabs.com/google-cloud-certified-professional-cloud-developer/

4. [video instructivo hecho por mí](https://www.tiktok.com/@mundo.devops/video/7296242376123616517?lang=es)

### Software de monitoreo
1. Este Software solo está para Windows, se descarga en la página oficial

2. [video instructivo hecho por mí](https://www.tiktok.com/@mundo.devops/video/7306555936515034374?lang=es)

### Preparación del ambiente
- Importante estar en un espacio tranquilo donde puedas estar concentrado
- No puedes hablar ni murmurar
- No se permite tener un monitor externo
- Una laptop idealmente
- Una webcam externa se permite si tu laptop no tiene integrada
- Se permite un mouse
- El área debe estar despejada, ya que te van a pedir que se las enseñes

## Temas que me aparecieron en la prueba
- Gke
- Cloud run
- Cloud Build
- Pubsub, tipos de suscripciones
- Permisos (IAM)
- Traffic director
- APP Engine
- Cloud Function
- VPC

## Cantidad de preguntas
**60 preguntas**, más que DevOps, Arq. y Asociado
    
## Un solo intento
Estas certificaciones de GCP son complejas y no te dan un re intento. Solo tienes una oportunidad
Por eso ten en cuenta la fecha para el examen, tomate tu tiempo y si no estas seguro puedes hacer una reprogramación antes de los 3 días de la fecha del examen, hasta ahi te deja reagendar

## Duración
El examen dura 2h

## Resultados 
1. Esperar los resultados de 7-10 días

2. [video instructivo hecho por mí](https://www.tiktok.com/@mundo.devops/video/7306959916495850757?lang=es)
